<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Movie extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['producer_id', 'name', 'year_of_release', 'plot', 'poster'];

    /**
     * Return full url of poster image.
     *
     * @param  string  $value
     * @return string
     */
    public function getPosterAttribute($value)
    {
        return Storage::disk('public')->url('posters/' . $value);
    }

    /**
     * Movie can have multiple Actor.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function actors()
    {
        return $this->belongsToMany(Actor::class);
    }

    /**
     * Movie has only one Producer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producer()
    {
        return $this->belongsTo(Producer::class);
    }
}
