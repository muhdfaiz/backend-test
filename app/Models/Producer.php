<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Producer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'sex', 'dob', 'bio'];

    /**
     * Producer can produce multiple movies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function movies()
    {
        return $this->hasMany(Movie::class);
    }
}
