<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'sex', 'dob', 'bio'];

    /**
     * Actor can act in multiple movies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany(Movie::class);
    }
}
