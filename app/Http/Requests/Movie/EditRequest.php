<?php

namespace App\Http\Requests\Movie;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:movies,name,' . $this->route('movie')->id,
            'year_of_release' => 'required|date_format:"Y"',
            'plot' => 'required',
            'poster' => 'nullable|image|mimes:jpeg,jpg,png,gif|max:2000',
            'producer' => 'required',
            'actors' => 'required',
        ];
    }
}
