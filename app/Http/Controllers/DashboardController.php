<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\Movie;
use App\Models\Producer;

class DashboardController extends Controller
{
    /**
     * Dashboard page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $totalActor = Actor::select('id')->count();

        $totalMovie = Movie::select('id')->count();

        $totalProducer = Producer::select('id')->count();

        return view('pages.dashboard.index', compact('totalActor', 'totalMovie', 'totalProducer'));
    }
}
