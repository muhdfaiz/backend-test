<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Http\Controllers;

use App\Http\Requests\Movie\CreateRequest;
use App\Http\Requests\Movie\EditRequest;
use App\Models\Actor;
use App\Models\Movie;
use App\Models\Producer;
use App\Services\MovieService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * @var MovieService
     */
    protected $movieService;

    /**
     * MovieController constructor.
     *
     * @param MovieService $movieService
     */
    public function __construct(MovieService $movieService)
    {
        $this->movieService = $movieService;
    }

    /**
     * List all movie available.
     */
    public function index()
    {
        $movies = Movie::with('actors', 'producer')->get();

        return view('pages.movies.index', compact('movies'));
    }

    /**
     * Use to get all movies. Used in ajax request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        $pageNumber = $request->query('page_number', 1);
        $pageLimit = $request->query('page_limit', 10);
        $keyword = $request->query('search', '');

        $movies = $this->movieService->getMovies($pageNumber, $pageLimit, $keyword);

        return response()->json($movies);
    }

    /**
     * Display details of movie.
     *
     * @param Movie $movie
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Movie $movie)
    {
        return view('pages.movies.show', compact('movie'));
    }

    /**
     * Display form to create new movie.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $actors = Actor::select(['id', 'name'])->get();

        $producers = Producer::select(['id', 'name'])->get();

        return view('pages.movies.create', compact('actors', 'producers'));
    }

    /**
     * Store new movie.
     *
     * @param CreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        if ($this->movieService->storeNewMovie($request->all())) {
            return response()->json(['message' => 'Successfully created new movie.']);
        }

        return response()->json(['error' => 'Failed to create new movie.'], 500);
    }

    /**
     * Show page to edit movie.
     *
     * @param Movie $movie
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Movie $movie)
    {
        $movie->load(['actors' => function ($query) {
            $query->select(['id', 'name']);
        }, 'producer' => function ($query) {
            $query->select(['id', 'name']);
        }]);

        $movie->year_of_release = Carbon::createFromFormat('Y', $movie->year_of_release)->format('Y-m-d');
        $actors = Actor::select(['id', 'name'])->get();

        $producers = Producer::select(['id', 'name'])->get();

        return view('pages.movies.edit', compact('movie', 'actors', 'producers'));
    }

    /**
     * Update movie
     */
    public function update(EditRequest $request, Movie $movie)
    {
        if ($this->movieService->updateMovie($movie, $request->all())) {
            return response()->json(['message' => 'Successfully updated movie.']);
        }

        return response()->json(['error' => 'Failed to update movie.'], 500);
    }

    /**
     * Delete movie
     *
     * @param Movie $movie
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Movie $movie)
    {
        $movieName = $movie->name;

        if ($this->movieService->deleteMovie($movie)) {
            return response()->json(['message' => 'Successfully deleted movie ' . strtolower($movieName) ]);
        }

        return response()->json(['error' => 'Failed to delete movie.'], 500);
    }
}
