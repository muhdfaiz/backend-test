<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Http\Controllers;

use App\Http\Requests\Actor\CreateRequest;
use App\Services\ActorService;

class ActorController extends Controller
{
    /**
     * @var ActorService
     */
    protected $actorService;

    /**
     * ActorController constructor.
     *
     * @param ActorService $actorService
     */
    public function __construct(ActorService $actorService)
    {
        $this->actorService = $actorService;
    }

    /**
     * Store new actor.
     *
     * @param CreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        if ($newActor = $this->actorService->storeNewActor($request->all())) {
            return response()->json([
                'data' => $newActor,
                'message' => 'Successfully created new actor.'
            ]);
        }

        return response()->json(['error' => 'Failed to create new actor.'], 500);
    }
}
