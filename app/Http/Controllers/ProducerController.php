<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Http\Controllers;

use App\Http\Requests\Producer\CreateRequest;
use App\Services\ProducerService;

class ProducerController extends Controller
{
    /**
     * @var ProducerService
     */
    protected $producerService;

    /**
     * ProducerController constructor.
     *
     * @param ProducerService $producerService
     */
    public function __construct(ProducerService $producerService)
    {
        $this->producerService = $producerService;
    }

    /**
     * Store new producer.
     *
     * @param CreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {
        if ($newProducer = $this->producerService->storeNewProducer($request->all())) {
            return response()->json([
                'data' => $newProducer,
                'message' => 'Successfully created new producer.'
            ]);
        }

        return response()->json(['error' => 'Failed to create new producer.'], 500);
    }
}
