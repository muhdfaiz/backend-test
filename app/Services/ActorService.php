<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Services;

use App\Models\Actor;

class ActorService
{
    /**
     * Store new actor in database.
     *
     * @param array $inputs
     *
     * @return mixed
     */
    public function storeNewActor(array $inputs)
    {
        $data = [
            'name' => $inputs['name'],
            'sex' => $inputs['sex'],
            'dob' => $inputs['dob'],
            'bio' => $inputs['bio'],
        ];

        $newActor = Actor::create($data);

        if ($newActor) {
            return $newActor;
        }

        return null;
    }
}
