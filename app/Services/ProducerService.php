<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Services;

use App\Models\Producer;

class ProducerService
{
    /**
     * Store new producer in database.
     *
     * @param array $inputs
     *
     * @return mixed
     */
    public function storeNewProducer(array $inputs)
    {
        $data = [
            'name' => $inputs['name'],
            'sex' => $inputs['sex'],
            'dob' => $inputs['dob'],
            'bio' => $inputs['bio'],
        ];

        $newProducer = Producer::create($data);

        if ($newProducer) {
            return $newProducer;
        }

        return null;
    }
}
