<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

namespace App\Services;

use App\Models\Movie;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MovieService
{
    /**
     * Get all movies matched the criteria
     *
     * @param int $pageNumber
     * @param int $pageLimit
     * @param $keyword
     *
     * @return mixed
     */
    public function getMovies(int $pageNumber, int $pageLimit, $keyword)
    {
        if ($keyword) {
            $movies = Movie::with('actors', 'producer')->where('name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('year_of_release', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('producer', function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                })->orWhereHas('actors', function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                })->paginate($pageLimit, ['*'], 'page_number', $pageNumber);

            return $movies;
        }

        $movies = Movie::with('actors', 'producer')->paginate($pageLimit, ['*'], 'page_number', $pageNumber);

        return $movies;
    }

    /**
     * Store new movie in database.
     *
     * @param array $inputs
     *
     * @return mixed
     */
    public function storeNewMovie(array $inputs)
    {
        $poster = '';

        if (isset($inputs['poster']) && $inputs['poster'] instanceof UploadedFile) {
            $poster = $this->uploadNewPoster($inputs['poster']);
        }

        $newMovie = DB::transaction(function() use ($inputs, $poster) {
            $data = [
                'name' => $inputs['name'],
                'year_of_release' => $inputs['year_of_release'],
                'plot' => $inputs['plot'],
                'producer_id' => $inputs['producer'],
                'poster' => $poster
            ];

            $newMovie = Movie::create($data);

            if ($newMovie) {
                $actors = explode(',', $inputs['actors']);

                $newMovie->actors()->attach($actors);
            }

            return $newMovie;
        });

        return $newMovie;
    }

    /**
     * Store new movie in database.
     *
     * @param Movie $movie
     * @param array $inputs
     *
     * @return mixed
     */
    public function updateMovie(Movie $movie, array $inputs)
    {
        $poster = '';
        if (isset($inputs['poster']) && $inputs['poster'] && $inputs['poster'] instanceof UploadedFile) {
            $poster = $this->uploadNewPoster($inputs['poster']);
        }

        $movie = DB::transaction(function() use ($movie, $inputs, $poster) {
            $oldPoster = $movie->getOriginal('poster');

            $movie->name = $inputs['name'];
            $movie->year_of_release = $inputs['year_of_release'];
            $movie->plot = $inputs['plot'];
            $movie->producer_id = $inputs['producer'];

            if ($poster) {
                $movie->poster = $poster;
            }

            if (!$movie->save()) {
                return false;
            }

            if ($poster) {
                Storage::disk('public')->delete('posters/' . $oldPoster);
            }

            $actors = explode(',', $inputs['actors']);

            $movie->actors()->sync($actors);

            return $movie;
        });

        return $movie;
    }

    /**
     * Upload new movie poster in storage.
     *
     * @param UploadedFile $poster
     *
     * @return string
     */
    protected function uploadNewPoster(UploadedFile $poster)
    {
        $poster = Storage::disk('public')->put('posters', $poster, 'public');

        return basename($poster);
    }

    /**
     * Delete movie including the poster.
     *
     * @param Movie $movie
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteMovie(Movie $movie)
    {
        $poster = $movie->getOriginal('poster');

        $result = DB::transaction(function() use ($movie, $poster) {
            $movie->actors()->detach();

            if (!$movie->delete()) {
                return false;
            }

            Storage::disk('public')->delete('posters/' . $poster);

            return true;
        });

        return $result;
    }
}
