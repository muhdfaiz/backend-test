<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

use Faker\Generator as Faker;
use App\Models\Actor;

$sex = ['male', 'female'];

$factory->define(Actor::class, function (Faker $faker) use ($sex) {
    return [
        'name' => $faker->name,
        'sex' => $sex[array_rand(['male', 'female'])],
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'bio' => $faker->text($maxNbChars = 300)
    ];
});
