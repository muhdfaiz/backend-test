<?php
/**
 * User: faiz
 * Date: 24/03/2019
 */

use Faker\Generator as Faker;
use App\Models\Producer;

$sex = ['male', 'female'];

$factory->define(Producer::class, function (Faker $faker) use ($sex) {
    return [
        'name' => $faker->name,
        'sex' => $sex[array_rand(['male', 'female'])],
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'bio' => $faker->text($maxNbChars = 300)
    ];
});
