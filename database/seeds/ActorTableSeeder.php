<?php

use Illuminate\Database\Seeder;
use App\Models\Actor;

class ActorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Actor::class, 10)->create();
    }
}
