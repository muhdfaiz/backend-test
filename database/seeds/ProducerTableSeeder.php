<?php

use Illuminate\Database\Seeder;
use App\Models\Producer;

class ProducerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Producer::class, 10)->create();
    }
}
