@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <create-movie-component :create_movie_url="'{{ route('movies.store')}}'" :create_actor_url="'{{ route('actors.store')}}'" :create_producer_url="'{{ route('producers.store')}}'"
            :actors="{{ $actors }}" :producers="{{ $producers }}"></create-movie-component>
    </div>
@endsection
