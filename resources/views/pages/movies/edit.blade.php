@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <edit-movie-component :update_movie_url="'{{ route('movies.update', [$movie->id])}}'" :movie="{{ $movie }}" :actors="{{ $actors }}" :producers="{{ $producers }}"></edit-movie-component>
    </div>
@endsection
