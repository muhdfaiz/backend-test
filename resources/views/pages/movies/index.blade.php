@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <list-movie-component :get_movie_url="'{{ route('movies.get') }}'"></list-movie-component>
    </div>
@endsection
