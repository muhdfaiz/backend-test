@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <b>Movie Details</b>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{ $movie->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Year Of Release</td>
                                        <td>{{ $movie->year_of_release }}</td>
                                    </tr>
                                    <tr>
                                        <td>Plot</td>
                                        <td>{{ $movie->plot }}</td>
                                    </tr>
                                    <tr>
                                        <td>Poster</td>
                                        <td><img style="width: 200px;" src="{{ $movie->poster }}"></td>
                                    </tr>
                                    <tr>
                                        <td>Created At</td>
                                        <td>{{ $movie->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td>Updated At</td>
                                        <td>{{ $movie->updated }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <b>Producer</b>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{ $movie->producer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Sex</td>
                                        <td>{{ $movie->producer->sex }}</td>
                                    </tr>
                                    <tr>
                                        <td>Date Of Birth</td>
                                        <td>{{ $movie->producer->dob }}</td>
                                    </tr>
                                    <tr>
                                        <td>Bio</td>
                                        <td>{{ $movie->producer->bio }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <b>List Of Actor</b>
                    </div>
                    <div class="card-body">
                        @foreach ($movie->actors as $key => $actor)
                            <div style="font-size: 16px;" class="mb-3 badge badge-primary">Actor {{ $key + 1 }}</div>
                            <div class="table-responsive mb-4">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{ $actor->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Sex</td>
                                        <td>{{ $actor->sex }}</td>
                                    </tr>
                                    <tr>
                                        <td>Date Of Birth</td>
                                        <td>{{ $actor->dob }}</td>
                                    </tr>
                                    <tr>
                                        <td>Bio</td>
                                        <td>{{ $actor->bio }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
